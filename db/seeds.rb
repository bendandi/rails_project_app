# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name:  "Example User",
             email: "example@dummy.com",
             password:              "foobar",
             password_confirmation: "foobar"
             )

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@dummy.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end


users = User.order(:created_at).take(1)
10.times do
  project_name = Faker::Lorem.words(rand(1..4)).join(" ")
  project_description = Faker::Lorem.sentence(10)
  users.each { |user| user.projects.create!(name: project_name, 
                                          description: project_description)}
end

10.times do
  users.each do |user|
    user.projects.each do |project|
      task_name = Faker::Lorem.words(rand(1..4)).join(" ")
      task_description = Faker::Lorem.sentence(10)
      project.tasks.create!(name: task_name, 
                            description: task_description,
                            progress_state: rand(0..2) )
    end
  end
  
end


