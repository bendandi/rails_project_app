class TasksController < ApplicationController
  
  def create
    @project = Project.find(params[:task][:project_id])
    @task = @project.tasks.build(task_params)
    if @task.save
      flash[:success] = "Task added to project"
      redirect_to request.referrer
    else
      redirect_to request.referrer
    end
  end
  
  def update
    @task = Task.find(params[:id])
    origin = Integer(@task[:progress_state])
    destination = Integer(params[:destination])
    if  ((origin - destination).abs == 1 && (0..2) === destination)
      if @task.update_attribute(:progress_state, Task.progress_states.key(destination)) 
        respond_to do |format|
          format.html { render @task }
          format.js
        end
      end
    end
  end
  
  def destroy
    Task.find(params[:id]).destroy
    flash[:success] = "Task Deleted"
    redirect_to request.referrer
  end
  
  private
    def task_params
      params.require(:task).permit(:name, :description, :progress_state, :project_id)
    end
end
